const express = require('express');
const path = require('path');
const app = express();

const greeter = require('./routes/greeter');

app.enable('strict routing');

// -------------
// Server routes
app.use('/greeter', greeter);

// -------------
// Client routes
app.use(express.static(path.join(__dirname, 'public')));
app.use('*', express.static(path.join(__dirname, 'public')));


// -------------
// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// -------------
// Global error handler
app.use((err, req, res) => {
  res.locals.message = err.message;
  res.locals.error = /dev/i.test(req.app.get('env')) ? err : {};

  res.status(err.status || 500);
});

// -------------
// App globals
app.set('port', process.env.PORT || 3000);
app.set('env', process.env.NODE_ENV || 'prod');

// -------------
// Starting app
app.listen(app.get('port'));
