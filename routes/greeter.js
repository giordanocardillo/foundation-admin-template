const express = require('express');
const router = express.Router();


// -------------
// Routes
router.get('/greet', (req, res) => {
  res.json({ message: 'Hello!' });
});

// -------------
// Handle not found
router.get('*', (req, res) => {
  const err = new Error('Not Found');
  res.status(404).json({
    error: 'Not found',
  });
});

module.exports = router;
