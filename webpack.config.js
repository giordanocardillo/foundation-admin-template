const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const extractSASS = new ExtractTextPlugin({
  filename: 'bundle/[name].min.css',
  allChunks: true,
});

const cleanWebpack = new CleanWebpackPlugin('bundle/*', {
  root: path.resolve(__dirname, 'public'),
});

module.exports = {
  context: path.resolve(__dirname, 'client'),
  entry: {
    app: './entry.js',
  },
  output: {
    filename: 'bundle/[name].min.js',
    path: path.resolve(__dirname, 'public'),
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|public)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
          },
        },
      }, {
        test: /\.(hbs|handlebars)$/,
        exclude: /(node_modules|public)/,
        loader: 'handlebars-loader',
      },
      {
        test: /\.(ttf|eot|svg|woff2?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: 'bundle/fonts/[name].[ext]',
        },
      },
      {
        test: /\.s?css$/,
        loader: extractSASS.extract({
          publicPath: '/',
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                minimize: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                minimize: true,
              },
            },
          ],
        }),
      },
    ],
  },
  plugins: [
    cleanWebpack,
    new webpack.optimize.UglifyJsPlugin({
      comments: false,
      mangle: true,
      sourceMap: true,
    }),
    extractSASS,
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks(module) {
        return module.context && /node_modules/.test(module.context);
      },
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery',
    }),
    new LiveReloadPlugin({
      appendScriptTag: true,
    }),
  ],
  devtool: /dev/i.test(process.env.NODE_ENV) ? 'source-map' : undefined
};
