const page = require('page')

// -------------
// Templates
const indexTemplate = require('./templates/index.hbs');
const notFoundTemplate = require('./templates/not-found.hbs');

module.exports = () => {
  page('/', () => {
    $('.container').html(indexTemplate);
  });

  page('*', () => {
     $('.container').html(notFoundTemplate);
     page.stop();
  });
  page();
}
